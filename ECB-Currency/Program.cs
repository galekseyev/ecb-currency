﻿using System;
using System.Xml;

namespace ECB_Currency
{
    class Program
    {
        static void Main(string[] args)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(@"http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");

            XmlNodeList xmlNodes = xmlDoc.SelectNodes("//*[@currency]");

            if (xmlNodes != null)
            {
                foreach (XmlNode node in xmlNodes)
                {
                    string currency = node.Attributes["currency"].Value;
                    decimal value = Convert.ToDecimal(node.Attributes["rate"].Value);

                    Console.WriteLine($"{currency} {value:#.##}");

                }

                Console.ReadLine();
            }
        }
    }
}
